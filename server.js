const express = require("express")
const WebSocket = require("ws")
const http = require("http")
const Viewpoint = require("./viewpointHelper").Viewpoint

// this is the class that holds multiple views on the server
class MultiViewer {
  constructor() {
    this.clients = new Array()
    this.idx = 0
  }

  initView(ws) {
    this.idx++
    let newViewpoint = new Viewpoint(ws, "server", this.idx).serverInit(this)
    this.clients.push(newViewpoint)
  }

  // when server obj gets new view this sends out to the connected viewpoint on browser
  updateOthers(serverIdx, serverView) {
    this.clients.forEach((client) => {
      if (client.idx != serverIdx) {
        client._setView(serverView)

        client.ws.send(
          JSON.stringify({
            messageType: "updateView",
            idx: client.idx,
            ...client.view,
          })
        )
      }
    })
  }
}

const multiview = new MultiViewer()

// express related
const app = express()
const port = process.env.PORT || 8080
const httpServer = http.createServer(app)
const wss = new WebSocket.Server({ server: httpServer })

app.use(express.static(__dirname))

app.get("/", (req, res) => {
  res.sendFile("index.html", { root: __dirname })
})

httpServer.listen(port, () => {
  console.log("Server started at http://localhost:" + port)
})

wss.on("open", function () {
  console.log("got open")
})

wss.on("connection", (client) => {
  multiview.initView(client)
})
