const geometryInit = { tileSize: 512, size: 512 }
const sourceInit = "//www.marzipano.net/media/cubemap/{f}.jpg"
const limiterInit = [4096, (100 * Math.PI) / 180]
const initalViewInit = {
  yaw: (90 * Math.PI) / 180,
  pitch: (-30 * Math.PI) / 180,
  fov: (90 * Math.PI) / 180,
}

const initialVals = {
  source: sourceInit,
  geometry: geometryInit,
  limiter: limiterInit,
  initialView: initalViewInit,
}

/**
 * Viewpoint is the class in browser + server that maintains view,
 * each viewpoint should have a Viewpoint instance on server and a Viewpoint instance on browser
 * connected via websocket
 *
 * back-and-forth is like:
 * marzipano event listener for
 */
class Viewpoint {
  constructor(ws, viewType = null, idx = -1) {
    this.ws = ws
    this.viewType = viewType
    this.idx = idx

    // defaults
    this._sending_params = false
    this.timer = undefined
    this.scene = null
    this.view = {
      yaw: 0.0,
      pitch: 0.0,
      roll: 0.0,
      fov: 0.0,
      width: 0,
      height: 0,
    }
  }

  _setView(view) {
    this.view = {
      yaw: view.yaw,
      pitch: view.pitch,
      roll: view.roll,
      fov: view.fov,
      width: view.width,
      height: view.height,
    }
  }

  browserInit() {
    this.viewer = new Marzipano.Viewer(document.getElementById("pano"))
    this.initEventListeners()
    return this
  }

  serverInit(multiview) {
    this.multiview = multiview
    this.ws.send(
      JSON.stringify({
        messageType: "initViewpoint",
        idx: this.idx,
        ...initialVals,
      })
    )
    this.initEventListeners()
    return this
  }

  /**
   * EVENT LISTENERS FOR MESSAGE PASSING
   */
  initEventListeners() {
    this.ws.addEventListener("message", (msg) => {
      let data = JSON.parse(msg.data)

      switch (data.messageType) {
        case "initViewpoint":
          this.initView(data)
          break
        case "updateView":
          this.updateView(data)
          break
        case "updateOthers":
          this.updateOthers(data)
          break
      }
    })
  }

  /**
   *
   * @param {*} param0
   */
  initView({ id, source, geometry, limiter, initialView }) {
    this.id = id
    this.source = Marzipano.ImageUrlSource.fromString(source)
    this.geometry = new Marzipano.CubeGeometry([geometry])
    this.limiter = Marzipano.RectilinearView.limit.traditional(...limiter)

    this.view = new Marzipano.RectilinearView(initialView, this.limiter)
    this.scene = this.viewer.createScene({
      source: this.source,
      geometry: this.geometry,
      view: this.view,
      pinFirstLevel: true,
    })

    this.scene.switchTo()

    // create callback for when we move the viewer in this browser instance
    this.scene.addEventListener("viewChange", () => {
      var params = this.scene.view()._params
      this._setView(params)
      this.debounce(() => {
        this.sendView()

        // try some semaphore type thing for when we send
        this._sending_params = true
        setTimeout(() => {
          this._sending_params = false
        }, 500)
      }, 50)()
    })
  }

  sendView() {
    // if (this.ws.readyState === 1) {}
    this.ws.send(
      JSON.stringify({
        messageType: "updateOthers",
        ...this.view,
      })
    )
  }

  // this is received on server and should update the other viewpoints on server
  // which then update their corresponding view on browser
  updateOthers(data) {
    this._setView(data)
    this.multiview.updateOthers(this.idx, data)
  }

  // this is where you got a new view from the server which got it from another browser
  updateView(data) {
    if (this._sending_params) return
    this._setView(data)
    this.debounce(() => {
      this.scene.lookTo({
        yaw: this.view.yaw,
        pitch: this.view.pitch,
        fov: this.view.fov,
      })
    })()
  }

  /**
   * need some way to slow when the viewChange callback is called
   * @param {*} func
   * @param {*} timeout
   * @returns
   */
  debounce(func, timeout = 100) {
    return (...args) => {
      if (!this.timer) {
        func.apply(this, args)
      }
      clearTimeout(this.timer)
      this.timer = setTimeout(() => {
        this.timer = undefined
      }, timeout)
    }
  }
}

// IDK how to do this best for js on front+backend
if (
  this &&
  typeof module == "object" &&
  module.exports &&
  this === module.exports
) {
  module.exports = { Viewpoint }
}
